# Todo App
Play around with GraphQL, Apollo Client and jest tests

## Get Started
### Server
In `/server`
```
yarn
yarn start
```

### Client
In `/mobile`
```
yarn
react-native run-android
```

## Authors:
Mateusz Siniarski - Developer
