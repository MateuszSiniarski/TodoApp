import React from 'react'
import renderer from 'react-test-renderer'

import Button from 'app/components/Button'
import Header from 'app/components/Header'
import InputText from 'app/components/InputText'
import Loading from 'app/components/Loading'
import Screen from 'app/components/Screen'

test('Button', () => {
  const tree = renderer.create(
    <Button
      text='Test Text'
      onPress={() => console.log('Test')}
    />
  ).toJSON()
  expect(tree).toMatchSnapshot()
})

test('Header', () => {
  const tree = renderer.create(
    <Header
      title='Test Title'
      left='Left Text'
      right='Right Text'
      onPressLeft={() => console.log('Left Press')}
      onPressRight={() => console.log('Right Press')}
    />
  ).toJSON()
  expect(tree).toMatchSnapshot()
})

test('InputText', () => {
  const tree = renderer.create(
    <InputText
      value='Some value'
      onChange={(value) => console.log(`Value Changed = ${value}`)}
    />
  ).toJSON()
  expect(tree).toMatchSnapshot()
})

test('Loading', () => {
  const tree = renderer.create(
    <Loading />
  ).toJSON()
  expect(tree).toMatchSnapshot()
})

test('Screen', () => {
  const tree = renderer.create(
    <Screen />
  ).toJSON()
  expect(tree).toMatchSnapshot()
})
