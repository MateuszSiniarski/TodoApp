import React from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity } from 'react-native'

import { color, common, constant, font } from 'app/styles'

class Button extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.props.onPress}
      >
        <Text style={styles.text}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}

export default Button

const styles = {
  container: {
    ...common.center,
    minHeight: constant.size.base,
    borderWidth: constant.thickness.base,
    borderColor: color.secondary
  },
  text: {
    ...font.base
  }
}
