import React from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity, View } from 'react-native'

import { color, constant } from 'app/styles'

class Header extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    left: PropTypes.string,
    right: PropTypes.string,
    onPressLeft: PropTypes.func,
    onPressRight: PropTypes.func
  }

  render() {
    const { title, left, right, onPressLeft, onPressRight } = this.props

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.left}
          disabled={!onPressLeft}
          onPress={onPressLeft}
        >
          <Text>{left}</Text>
        </TouchableOpacity>
        <Text style={styles.middle}>{title}</Text>
        <TouchableOpacity
          style={styles.right}
          disabled={!onPressRight}
          onPress={onPressRight}
        >
          <Text>{right}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default Header

const column = {
  flex: 1,
  justifyContent: 'center',
  marginHorizontal: constant.space.base
}

const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: constant.size.base,
    borderBottomColor: color.primary,
    borderBottomWidth: constant.thickness.base
  },
  column,
  left: {
    ...column,
    alignItems: 'flex-start'
  },
  middle: {
    ...column,
    textAlign: 'center'
  },
  right: {
    ...column,
    alignItems: 'flex-end'
  }
}
