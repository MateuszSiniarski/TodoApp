import React from 'react'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native'

import { color, constant } from 'app/styles'

class InputText extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
  }

  render() {
    const { value, onChange } = this.props

    return (
      <TextInput
        style={styles.input}
        value={value}
        onChangeText={onChange}
      />
    )
  }
}

export default InputText

const styles = {
  input: {
    minHeight: constant.size.base,
    borderColor: color.neutral,
    borderWidth: constant.thickness.base
  }
}
