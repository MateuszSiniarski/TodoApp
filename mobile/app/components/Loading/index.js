import React from 'react'
import { ActivityIndicator } from 'react-native'

import { color } from 'app/styles'

class Loading extends React.Component {
  render() {
    return (
      <ActivityIndicator
        size='large'
        color={color.primary}
      />
    )
  }
}

export default Loading
