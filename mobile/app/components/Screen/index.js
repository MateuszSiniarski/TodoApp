import React from 'react'
import PropTypes from 'prop-types'
import { SafeAreaView } from 'react-navigation'

import { color } from 'app/styles'

class Screen extends React.Component {
  static propTypes = {
    style: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object
    ]),
    children: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object
    ])
  }

  render() {
    return (
      <SafeAreaView style={[styles.container, this.props.style]}>
        {this.props.children}
      </SafeAreaView>
    )
  }
}

export default Screen

const styles = {
  container: {
    flex: 1,
    backgroundColor: color.neutral
  }
}
