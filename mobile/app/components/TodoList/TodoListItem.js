import React from 'react'
import PropTypes from 'prop-types'
import { Text } from 'react-native'

class TodoListItem extends React.Component {
  static propTypes = {
    item: PropTypes.object.isRequired
  }

  render() {
    const { description } = this.props.item

    return (
      <Text>{description}</Text>
    )
  }
}

export default TodoListItem
