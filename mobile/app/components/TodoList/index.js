import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { connect } from 'react-redux'

import { actions as todoActions } from 'app/reducers/todo'

import TodoListItem from './TodoListItem'

class TodoList extends React.Component {
  static propTypes = {
    style: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object
    ]),
    todo: PropTypes.object.isRequired,
    loadTodos: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.loadTodos()
  }

  renderItem = (item) =>
    <TodoListItem
      key={item.id}
      item={item}
    />

  render() {
    const { items } = this.props.todo

    return (
      <View style={[styles.container, this.props.style]}>
        {items.map(this.renderItem)}
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  todo: state.todo
})

const mapDispatchToProps = {
  ...todoActions
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(TodoList)
export default Connected

const styles = {
  container: {
    flex: 1
  }
}
