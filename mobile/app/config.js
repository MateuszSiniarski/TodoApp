import { Platform } from 'react-native'

const getApiBase = () => {
  switch (process.env.NODE_ENV) {
    case undefined:
    case 'development':
      return Platform.select({
        ios: 'http://localhost:4000/graphql',
        android: 'http://10.0.2.2:4000/graphql' // Android Emulator pass AVD localhost
      })
    default:
      throw new Error(`Invalid NODE_ENV=${process.env.NODE_ENV}`)
  }
}

const config = {
  apiBase: getApiBase()
}

export default config
