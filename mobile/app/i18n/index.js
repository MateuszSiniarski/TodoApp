import i18next from 'i18next'
import { reactI18nextModule } from 'react-i18next'

import eng from './locales/eng'

export const defaultOptions = {
  bindI18n: 'languageChanged',
  bindStore: false
}

i18next
  .use(reactI18nextModule)
  .init({
    fallbackLng: 'eng',
    lng: 'eng',
    debug: false,
    resources: {
      eng
    }
  })

export default i18next
