/* eslint max-len: "off" */

export default {
  login: {
    login: 'Login'
  },
  todoList: {
    title: 'Todo List',
    add: 'Add Todo'
  },
  todoAdd: {
    title: 'Add Todo',
    back: 'Back',
    add: 'Create new Todo'
  },
  more: {
    logout: 'Logout'
  }
}
