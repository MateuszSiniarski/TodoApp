import React from 'react'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'

import reducers from 'app/reducers'
import AppNavigator from 'app/routes'
import i18n from 'app/i18n'

const middleware = [
  thunk
]

const store = createStore(
  combineReducers(reducers),
  applyMiddleware(...middleware)
)

const AppNavigatorWithI18n = () =>
  <AppNavigator screenProps={{ t: i18n.getFixedT() }} />

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigatorWithI18n />
      </Provider>
    )
  }
}
