import { reducer as todo } from './todo'

const reducers = {
  todo
}

export default reducers
