import { createTodo, loadTodos } from 'app/utils/api'

export const LOAD_TODOS = 'LOAD_TODOS'
export const ADD_TODO = 'TODO_ADD'

export const actions = {
  loadTodos: () => (dispatch) => {
    loadTodos()
      .then(result => {
        dispatch({ type: LOAD_TODOS, items: result })
      })
  },
  addTodo: (item) => (dispatch) => {
    createTodo(item)
      .then(result => {
        dispatch({ type: ADD_TODO, item: result })
      })
  }
}

const initialState = {
  items: []
}

const actionHandlers = {
  [LOAD_TODOS]: (state, { items }) =>
    ({
      ...state,
      items
    }),
  [ADD_TODO]: (state, { item }) =>
    ({
      ...state,
      items: [...state.items, item]
    })
}

export const reducer = (state = initialState, action) => {
  const handler = actionHandlers[action.type]

  return handler
    ? handler(state, action)
    : state
}
