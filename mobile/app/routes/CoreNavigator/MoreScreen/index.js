import React from 'react'
import PropTypes from 'prop-types'
import { translate } from 'react-i18next'

import Screen from 'app/components/Screen'
import Button from 'app/components/Button'

class MoreScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  }

  handleLogout = () =>
    this.props.navigation.navigate('Login')

  render() {
    const { t } = this.props

    return (
      <Screen>
        <Button
          text={t('logout')}
          onPress={this.handleLogout}
        />
      </Screen>
    )
  }
}

const Translated = translate('more')(MoreScreen)
export default Translated
