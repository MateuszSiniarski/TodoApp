import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'

import { actions as todoActions } from 'app/reducers/todo'

import Screen from 'app/components/Screen'
import Header from 'app/components/Header'
import InputText from 'app/components/InputText'
import Button from 'app/components/Button'

class TodoAddScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired,
    addTodo: PropTypes.func.isRequired
  }

  constructor() {
    super()

    this.state = {
      description: ''
    }
  }

  handleBack = () =>
    this.props.navigation.goBack()

  handleAddTodo = () => {
    this.props.addTodo(this.state)
    this.props.navigation.goBack()
  }

  handleChange = (field) => (value) =>
    this.setState({ [field]: value })

  render() {
    const { t } = this.props

    return (
      <Screen>
        <Header
          title={t('title')}
          left={t('back')}
          onPressLeft={this.handleBack}
        />
        <InputText
          value={this.state.description}
          onChange={this.handleChange('description')}
        />
        <Button
          text={t('add')}
          onPress={this.handleAddTodo}
        />
      </Screen>
    )
  }
}

const mapDispatchToProps = {
  ...todoActions
}

const Connected = connect(null, mapDispatchToProps)(TodoAddScreen)
const Translated = translate('todoAdd')(Connected)
export default Translated
