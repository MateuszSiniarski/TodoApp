import React from 'react'
import PropTypes from 'prop-types'
import { translate } from 'react-i18next'

import Screen from 'app/components/Screen'
import Header from 'app/components/Header'
import TodoList from 'app/components/TodoList'

class TodoListScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  }

  handleNavigateAddTodo = () =>
    this.props.navigation.navigate('TodoAdd')

  render() {
    const { t } = this.props

    return (
      <Screen>
        <Header
          title={t('title')}
          right={t('add')}
          onPressRight={this.handleNavigateAddTodo}
        />
        <TodoList />
      </Screen>
    )
  }
}

const Translated = translate('todoList')(TodoListScreen)
export default Translated
