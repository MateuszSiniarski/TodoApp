import { createStackNavigator } from 'react-navigation'

import TodoListScreen from './TodoListScreen'
import TodoAddScreen from './TodoAddScreen'

const routeConfigMap = {
  TodoList: { screen: TodoListScreen },
  TodoAdd: { screen: TodoAddScreen }
}

const stackConfig = {
  initialRouteName: 'TodoList',
  headerMode: 'none'
}

const TodoNavigator = createStackNavigator(routeConfigMap, stackConfig)
export default TodoNavigator
