import { createBottomTabNavigator } from 'react-navigation'

import TodoNavigator from './TodoNavigator'
import MoreScreen from './MoreScreen'

const routeConfigMap = {
  Todo: { screen: TodoNavigator },
  More: { screen: MoreScreen }
}

const stackConfig = {
  initialRouteName: 'Todo',
  headerMode: 'none'
}

const CoreNavigator = createBottomTabNavigator(routeConfigMap, stackConfig)
export default CoreNavigator
