import React from 'react'
import PropTypes from 'prop-types'

import Screen from 'app/components/Screen'
import Loading from 'app/components/Loading'

import { common } from 'app/styles'

class LoadingScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Login')
    }, 1500) // Simulate loading delay
  }

  render() {
    return (
      <Screen style={styles.container}>
        <Loading />
      </Screen>
    )
  }
}

export default LoadingScreen

const styles = {
  container: {
    ...common.center
  }
}
