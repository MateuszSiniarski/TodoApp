import React from 'react'
import PropTypes from 'prop-types'
import { translate } from 'react-i18next'

import Screen from 'app/components/Screen'
import Button from 'app/components/Button'

class LoginScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  }

  handleLogin = () =>
    this.props.navigation.navigate('Todo')

  render() {
    const { t } = this.props

    return (
      <Screen style={styles.container}>
        <Button
          text={t('login')}
          onPress={this.handleLogin}
        />
      </Screen>
    )
  }
}

const Translated = translate('login')(LoginScreen)
export default Translated

const styles = {
  container: {
    justifyContent: 'flex-end'
  }
}
