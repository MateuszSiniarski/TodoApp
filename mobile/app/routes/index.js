import { createSwitchNavigator } from 'react-navigation'

import LoadingScreen from './LoadingScreen'
import LoginScreen from './LoginScreen'
import CoreNavigator from './CoreNavigator'

const routeConfigMap = {
  Loading: { screen: LoadingScreen },
  Login: { screen: LoginScreen },
  Core: { screen: CoreNavigator }
}

const stackConfig = {
  initialRouteName: 'Loading',
  headerMode: 'none'
}

const AppNavigator = createSwitchNavigator(routeConfigMap, stackConfig)
export default AppNavigator
