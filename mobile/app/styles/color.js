const color = {
  primary: '#AE0C09',
  secondary: '#7A7A32',
  neutral: '#B3B26F',
  white: '#FFFFFF',
  black: '#000000'
}

export default color
