const constant = {
  size: {
    base: 50
  },
  space: {
    base: 10
  },
  thickness: {
    thin: 1,
    base: 2,
    thick: 3
  }
}

export default constant
