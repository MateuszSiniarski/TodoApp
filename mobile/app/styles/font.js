const baseFont = {
  // color
  // fontFamily
}

const font = {
  base: {
    ...baseFont,
    fontSize: 16
  }
}

export default font
