import _color from './color'
import _common from './common'
import _constant from './constant'
import _font from './font'

export const color = _color
export const common = _common
export const constant = _constant
export const font = _font
