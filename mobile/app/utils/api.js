import { ApolloClient, ApolloLink, HttpLink, InMemoryCache } from 'apollo-boost'
import gql from 'graphql-tag'

import config from 'app/config'

const client = new ApolloClient({
  link: ApolloLink.from([
    new HttpLink({ uri: config.apiBase })
  ]),
  cache: new InMemoryCache()
})

const todoDetailFragment = gql`
  fragment TodoDetail on Todo {
    id
    description
  }
`

const createTodoMutation = gql`
  mutation CreateTodo($input: CreateTodoInput){
    todo: createTodo(input: $input) {
      ...TodoDetail
    }
  }
  ${todoDetailFragment}
`

const todoQuery = gql`
  query TodoQuery($id: ID!) {
    todo(id: $id) {
      ...TodoDetail
    }
  }
  ${todoDetailFragment}
`
const todosQuery = gql`
  query TodosQuery {
    todos {
      ...TodoDetail
    }
  }
  ${todoDetailFragment}
`

export const createTodo = async (input) => {
  const { data: { todo } } = await client.mutate({
    mutation: createTodoMutation,
    variables: { input }
  })
  return todo
}

export const loadTodo = async (id) => {
  const { data: { todo } } = await client.query({ query: todoQuery, variables: { id } })
  return todo
}

export const loadTodos = async () => {
  const { data: { todos } } = await client.query({ query: todosQuery })
  return todos
}
