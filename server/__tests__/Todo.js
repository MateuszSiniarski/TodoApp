const { mockServer } = require('graphql-tools')
const fs = require('fs')

const schema = fs.readFileSync('./src/schema.graphql', { encoding: 'utf-8' })

const TodoDescription = 'Todo Description'

const myMockServer = mockServer(
  schema,
  {
    Todo: () => ({
      description: () => TodoDescription
    })
  }
)

describe('Todos', () => {
  it('loads list of Todo', async () => {
    const result = await myMockServer.query(`{
      todos {
        id
        description
      }
    }`)

    const { data: { todos } } = result
    expect(Array.isArray(todos)).toBe(true)
    todos.map(ent => expect(ent.description).toBe(TodoDescription))
  })

  it('loads single Todo from Id', async () => {
    const result = await myMockServer.query(`{
      todo(id: 0) {
        id
        description
      }
    }`)

    const { data: { todo } } = result
    expect(Array.isArray(todo)).toBe(false)
    expect(todo.description).toBe(TodoDescription)
  })

  it('create returns Todo', async () => {
    const result = await myMockServer.query(`mutation CreateTodo($input: CreateTodoInput) {
      todo: createTodo(input: $input) {
        id
        description
      }
    }
    `, {
        input: {
          description: TodoDescription
        }
      })

    const { data: { todo } } = result
    expect(Array.isArray(todo)).toBe(false)
    expect(todo.description).toBe(TodoDescription)
  })
})
