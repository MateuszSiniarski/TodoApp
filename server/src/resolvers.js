const db = require('./db')

const Query = {
  todo: (root, { id }) =>
    db.todo.get(id),
  todos: () =>
    db.todos.list()
}

const Mutation = {
  createTodo: (root, { input }) => {
    const id = db.todos.create(input)
    return db.todos.get(id)
  }
}

const Todo = {}

module.exports = { Query, Mutation, Todo }
